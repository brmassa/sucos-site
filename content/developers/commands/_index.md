---
Title: Commands
Params:
  hasCode: true
---

This documentation describes the common flow shared by the SuCoS [build](build-command) and [serve](live-server-command) commands. These commands, executed through the SuCoS CLI, enable you to transform your source files into a fully generated website and serve it locally for a seamless development experience. Let's dive into the details of this magical process.

```mermaid
graph LR
A[1 Command Line] --> B{2 sucos.yml}
B --> C{3 Scan Content}
C --> D{4 Generate Extra}
D --> E[5 Build or Serve]

style E fill:#999;

click A "#step-1-command-line-arguments" "Link to Step 1"
click B "#step-2-read-and-parse-sucos.yml" "Link to Step 1"
click C "#step-3-read-and-parse-markdown-files" "Link to Step 1"
click D "#step-4-generate-front-matter-for-tags" "Link to Step 1"
```

Steps 1 to 4 are common to both commands, so we will describe them here.

## Step 1: Command Line Arguments

Both the build and serve commands begin with command line arguments that customize the website generation or server operation. The available options are:

* `--source` or `-s`: Specifies the source directory path. Use this option to provide a custom directory containing your website's source files. If not specified, the current directory is used as the source directory.
* `--verbose` or `-v`: Enables verbose output during the process. With this option, **SuCoS** provides detailed information and feedback, enhancing your understanding of the steps performed.
* `--help` or `-h` or `-?`: Show help and usage information.
* `--version`: Show version information (which in fact is displayed every time)

#### Basic build command

```bash
SuCoS build
```

#### Basic serve command

```bash
SuCoS serve
```

This will build or serve the site using the default source directory (`./`) and output the generated files to the default output directory (`./public`).

#### Enable verbose output

```bash
sucos build --verbose
```

## Step 2: Read and Parse sucos.yml

After setting the stage with command line arguments, **SuCoS** proceeds to read and parse the sucos.yml configuration file. This file, located in the source folder, holds the website's configuration details, such as the site title and chosen theme. Parsing this file ensures that **SuCoS** aligns perfectly with your desired website configuration.

## Step 3: Read and Parse Markdown Files

In the next stage, **SuCoS** embarks on a quest to read and parse the captivating Markdown files residing within the "content" folder. Each Markdown file represents a page on your website, containing valuable content, metadata, and front matter. **SuCoS** creates instances of the Frontmatter class, capturing essential information required for further processing.

## Step 4: Generate Front Matter for Tags

Following the parsing of Markdown files, **SuCoS** generates front matter for tags mentioned within the content. This process ensures seamless navigation and filtering capabilities on your website. Additionally, **SuCoS** weaves a special spell to create an extra "index.html" file, serving as the captivating homepage for your visitors.

## Step 5: Build (for the build command) or Serve (for the serve command)

For the build command, **SuCoS** proceeds to build the website. It organizes the content structure, applies the chosen theme, and generates HTML files based on the gathered data and configuration. Finally, **SuCoS** writes the complete website, including the homepage, into an output folder, ready to be hosted and shared with the world.

For the serve command, **SuCoS** takes a different approach. It keeps the metadata in memory instead of generating all the pages upfront. This dynamic page generation allows **SuCoS** to respond to requests for specific pages, ensuring efficient use of resources. The server monitors the source folder for changes, rescanning files and rebuilding the metadata as needed. To enhance the development experience, a JavaScript snippet is injected into the served pages, enabling automatic page reloading when the server restarts.

With these steps, the SuCoS build and serve commands unleash the magic of **SuCoS**, transforming your source files into a breathtakingly beautiful and fully functional website. Whether you choose to build your website or serve it locally, **SuCoS** empowers you to witness the enchantment firsthand. Embark on this extraordinary journey, and may your creations shine brightly in the digital realm. Happy building and serving!
