---
Title: Windows
Params:
  hasCode: true
---


This guide complements the installation guide, just giving the Windows-only stuff.

## Installer

Currently, SuCoS does not have any installer. It's a single executable file that can be placed anywhere on your system.

## Manual Installation

1. [Download](https://gitlab.com/sucos/sucos/-/releases) the SuCoS executable for Windows
2. Place it in your desired location (e.g., `C:\Program Files\SuCoS`)
3. (Optional) Add to PATH:
   - Right-click on 'This PC' or 'My Computer'
   - Click 'Properties'
   - Click 'Advanced system settings'
   - Click 'Environment Variables'
   - Under 'System Variables', find and select 'Path'
   - Click 'Edit'
   - Click 'New'
   - Add the path to your SuCoS folder (e.g., `C:\Program Files\SuCoS`)
   - Click 'OK' on all windows

## Package Managers

Please note that we currently do not have any pre-built releases for package managers such as *Chocolatey*, *Scoop*, or *Winget*.

However, we are continuously working to expand the installation options and plan to support package managers in the future.

## Running from Command Prompt

Once installed, you can run SuCoS from any Command Prompt or PowerShell window by typing `sucos`.
