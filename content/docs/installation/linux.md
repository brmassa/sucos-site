---
Title: Linux
Params:
  hasCode: true
---


This guide complements the installation guide, just giving the Linux-only stuff.

## Manual Installation

The application is distributed as a single executable. There are two main ways to install it:

### Option 1: System-wide Installation

Move the SuCoS executable to a standard binary location:

```sh
sudo mv ./SuCoS /usr/local/bin/sucos
sudo chmod +x /usr/local/bin/sucos
```

### Option 2: User-specific Installation

1. Create a bin directory in your home folder (if it doesn't exist):
```sh
mkdir -p ~/bin
```

2. Move SuCoS there:
```sh
mv ./SuCoS ~/bin/sucos
chmod +x ~/bin/sucos
```

3. Add to PATH (if not already done):
```sh
echo 'export PATH="$HOME/bin:$PATH"' >> ~/.bashrc
source ~/.bashrc
```

## Package Managers

Please note that we currently do not have any pre-built releases for repository packages such as *pacman*, *apt* or *dnf* neither package managers such as *Snap* or *Flatpak*.

However, we are continuously working to expand the installation options and plan to support package managers in the future.

"A Linux user myself, I am particularly interested in it" - Bruno Massa, main dev

## Troubleshooting

If you get a "Permission denied" error, ensure the executable has proper permissions:
```sh
chmod +x /path/to/sucos
```

For SELinux systems, you might need to update the security context:
```sh
sudo chcon -t bin_t /path/to/sucos
```
