---
Title: Content
Weight: 2
Params:
  hasCode: true
---

SuCoS uses Markdown files with front matter to organize and create content. This guide provides an overview of content creation basics.

## Content Structure

Content in SuCoS is organized in the `content` directory. Common structures include:

```tree
content/
├── _index.md        # Home page
├── pages/           # Section: Regular pages
│   └── about.md
├── blog/            # Section: Blog posts
│   ├── _index.md    # Blog listing page
│   ├── post-1.md
│   └── post-2.md
├────── post-3/      # Page bundle
│       ├── index.md
│       └── picture.jpg
└── docs/            # Section: Documentation
    └── getting-started.md
```

## Front Matter

Every content file starts with front matter - metadata about the page enclosed in `---`:

```yaml
---
Title: My First Post
Date: 2023-12-20
Tags:
  - tutorial
  - basics
Draft: false
---
```

Common front matter fields:
- `title`: Page title
- `date`: Publication date
- `tags`: Content categorization
- `draft`: If true, page won't be published
- `weight`: Controls content ordering

## Content Format

After the front matter, write your content in Markdown:

```md
---
title: Welcome to SuCoS
---

# Welcome

This is a paragraph with **bold** and *italic* text.

## Features

- Simple to use
- Fast building
- Flexible
```

## Tags and Categories

Tags help organize and group related content:

```yaml
---
Title: Advanced Features
Tags:
  - tutorial
  - advanced
---
```

Users can then browse content by tags or categories through automatically generated listing pages.

## Index Pages

Special `_index.md` files define section landing pages and control how content is listed:

```yaml
---
Title: Blog Posts
---

Welcome to my blog section!
```

## Draft Content

Mark content as draft to prevent publication:

```yaml
---
Title: Work in Progress
Draft: true
---
```

Use `sucos serve --drafts` to preview draft content locally.
