---
Title: Introduction
Weight: -10
---

Welcome to the world of **SuCoS**, a versatile website framework that combines functionality with brilliance. **SuCoS** is a static site generator (SSG) that revolutionizes website creation. It builds pages when you create or update your content, ensuring optimal viewing experiences for users and an ideal writing experience for authors.

In technical terms, **SuCoS** scans your content folder, converts it into a complete website, ready to be hosted anywhere. It simplifies the process by eliminating the need for databases or resource-intensive runtimes like Ruby, Python, or PHP. **SuCoS** is the ultimate website creation tool, providing instant build times and the ability to rebuild with ease.

## Who should use SuCoS?

Let's break it down:

**Text Editor Aficionados**: If you love writing in a text editor, **SuCoS** is your creative companion. Craft your content effortlessly while **SuCoS** transforms your ideas into a polished website.

**Website Builders**: Whether it's a blog, company site, portfolio, documentation, a single landing page, or a sprawling website, **SuCoS** caters to projects of all sizes and genres.

**Coding Enthusiasts**: Hand-code your website without the hassle of intricate runtimes, dependencies, and databases. **SuCoS** empowers you to focus on your code and design.

## Who should NOT use SuCoS?

But like any tool, SuCoS isn't for everyone. Consider the following scenarios where alternatives may be more suitable:

**Production-Ready Projects**: If you need a stable product for mission-critical projects, SuCoS is still in it's infancy.

**Advanced Dynamic Functionality**: **SuCoS** excels at static websites. If you require extensive dynamic features, such as user authentication or complex database-driven applications, other options might be more appropriate.

**Real-Time Content Updates**: If your website necessitates real-time content updates or frequent changes; use a CMS.

**SuCoS** continues to evolve, aiming to conquer new frontiers in website creation. The possibilities are endless, and the journey is bound to be unforgettable.
