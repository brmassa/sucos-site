---
Title: Theme
Weight: 5
Params:
  hasCode: true
---

SuCoS utilizes **Liquid** templates to dynamically generate web pages. Liquid is a simple and expressive templating language that allows for the insertion of dynamic content into templates. Each page will receive a full [Page object](/docs/theme/variables/page-variables) to you use it.

## Liquid Template Structure

The default structure of a Liquid template in SuCoS SSG typically includes the following elements:

```html
<!DOCTYPE html>
<html>
  <head>
    <!-- The current page and the site names-->
    <title>{{ page.Title }} - {{ page.Site.Title }}</title>
  </head>
  <body>
    <!-- List all tags-->
    <nav>
      <ul>
        {% assign pages = page.Site.Pages | where: 'Section', 'tags'%}
        {% for tag in pages %}
        <li><a href="{{ tag.Permalink }}">{{ tag.Title }}</a></li>
        {% endfor %}
      </ul>
    </nav>

    <main>
      <h1>{{ page.Title }}</h2>
      {{ page.Content }}
    </main>

    <footer>{{ page.Author }}</footer>
  </body>
</html>
```

## Liquid Syntax

Liquid templates use a set of tags, filters, and variables to render dynamic content. In **SuCoS**, the pages object is provided, which contains properties that can be accessed within the Liquid templates. For example, {{ page.Title }} retrieves the title of the current page, and {{ page.Site.Title }} retrieves the title of the entire website.

You can also use control flow statements in Liquid templates. The {% for ... %} and {% endfor %} tags, shown in the example above, are used to iterate over a collection of items.

For further information on Liquid syntax and available features, please refer to the official Liquid documentation at <https://shopify.github.io/liquid/>.
