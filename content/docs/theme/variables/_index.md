---
Title: Variables
---

One of the key features of SuCoS is the ability to expose variables that can be utilized within your theme templates. These variables provide dynamic content and enable you to create highly customizable and personalized websites. In SuCoS, there are two main blocks of variables: page variables and site variables. Let's explore each of them in detail:

## Page Variables

Page variables are specific to each individual page of your website. They provide access to information and metadata associated with the current page being rendered. These variables can be utilized to dynamically generate content based on the unique characteristics of each page. Some common page variables include:

* `{{ page.Title }}`: Represents the title of the current page.
* `{{ page.Permalink }}`: Provides the URL or path of the current page.
* `{{ page.Date }}`: Indicates the publication date of the page.
* `{{ page.Tags }}`: Provides a list of tags associated with the page.

You can access and use these variables within your theme templates to display page-specific information or create conditional logic based on the properties of the current page.

## Site Variables

Site variables, as the name suggests, are accessible throughout the entire website. All pages also points to the Site variables by using `{{ page.Site }}`. It contains information and metadata that is global to the entire site, such as site title, description, and navigation links. These variables allow you to maintain consistent elements across all pages of your website. Some common site variables include:

* `{{ page.Site.Title }}`: Represents the title of your website.

By utilizing site variables, you can dynamically generate the header, footer, or any other global element of your website, ensuring consistency and efficient maintenance.

## Using Variables in Theme Templates

To incorporate these variables within your theme templates, you can simply enclose them in double curly braces, like {{ variable_name }}. For example, to display the page title, you would use `{{ page.Title }}`.

SuCoS provides a flexible and intuitive way to leverage these variables, empowering you to create dynamic, personalized, and data-driven websites. By harnessing the power of page and site variables, you can easily customize your theme templates to suit your specific needs.

For more detailed information on available variables and their usage, please refer to the SuCoS SSG documentation or consult the relevant section of the user manual.

We hope this introductory guide has provided you with a solid understanding of SuCoS SSG's theme variables. Happy templating!
