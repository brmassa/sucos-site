---
Title: New Theme
Weight: 1
Params:
  hasCode: true
---

## Introduction

The `SuCoS new-theme` command allows you to create a simple folder structure to host your custom theme. This command generates a basic file and directory layout to help you get started with your SuCoS theme.

## Option Details

Here are the command line options available for the `new-theme` command:

- `--output` or `-o`: Specifies the output directory path. This is the location where SuCoS will create the new theme structure. If not specified, the default output directory is the current directory (`./`).

- `--force` or `-f`: Forces theme creation. Use this option to overwrite existing files or directories with the same name as the new theme structure.

- `--title`: Sets the theme title. This option allows you to specify the title of your theme, which will be used in the theme configuration file (`sucos.yml`). The default title is "My Theme".

## Directory Structure

When you run the `new-theme` command, SuCoS will create the following directory structure:

```tree
└── your-theme-name/
    ├── static/
    ├── layout/
    │   ├── _default.html
    │   └── partials/
    └── sucos.yml
```

- `static/`: This directory is used to store your theme's static files, such as images, CSS, and JavaScript files.
- `layout/`: This directory is used to store your theme's layout files. The `_default.html` file is the main layout that will be used by default for all content.
- `layout/partials/`: This directory is used to store your theme's partial layout files. Partials are reusable components that can be included in other layout files.
- `sucos.yml`: This file holds basic theme information and configuration settings. For more information, see the [Theme Settings](/docs/theme) manual.

## Command Line Usage Examples

Here are five examples showcasing the usage of the **SuCoS** new-theme command, demonstrating the combination of different arguments:

### Create a new theme with default settings

```bash
SuCoS new-theme
```

This command will create a new theme with default settings in the current directory.

### Create a new theme with a custom output directory

```bash
SuCoS new-theme -o ./my-theme
```

This command will create a new theme in the `./my-theme` directory.

### Create a new theme with a custom title

```bash
SuCoS new-theme --title "My Custom Theme"
```

This command will create a new theme with a custom title ("My Custom Theme").

### Create a new theme with force enabled

```bash
SuCoS new-theme -f
```

This command will create a new theme, overwriting any existing files or directories with the same name. Be cautious when using this option, as it may result in data loss.

### Create a new theme with a custom output directory and title

```bash
SuCoS new-theme -o ./my-theme --title "My Custom Theme"
```

This command will create a new theme in the `./my-theme` directory with a custom title ("My Custom Theme").
