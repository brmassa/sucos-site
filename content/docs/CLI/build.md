---
Title: Build
Params:
  hasCode: true
---

The `SuCoS build` command is a powerful tool that allows you to create the final version of your static site on your local disk. This command compiles all your source files, including content, templates, and assets, and generates a ready-to-deploy website.

## Option Details

Let's delve deeper into each available command line argument for the **SuCoS** build command:

- `--verbose` or `-v`: Enables verbose output. This option provides detailed information about the build process, which can be helpful for debugging or understanding what's happening behind the scenes.

- `--source` or `-s`: Specifies the source directory path. This is the location where SuCoS will look for your content, templates, and other site-related files. If not specified, the default source directory is the current directory (`./`).

- `--output` or `-o`: Specifies the output directory path. Use this option to set a custom directory where the generated website files will be saved. If not specified, the default output directory is `./public`.

- `--draft` or `-d`: Includes draft content in the build. By default, SuCoS ignores content marked as draft. Use this option if you want to include these files in your final site.

- `--future` or `-f`: Includes content with dates in the future. By default, SuCoS only includes content with dates in the past or present. Use this option to also include future-dated content.

- `--expired` or `-e`: Includes content with expired dates. By default, SuCoS ignores content whose expiration date is in the past. Use this option to include these files in your final site.

These options provide flexibility and customization to tailor the build command according to your specific requirements. You can combine them as needed to achieve the desired results when generating your **SuCoS** website.

## Command Line Usage Examples

Here are five examples showcasing the usage of the **SuCoS** build command, demonstrating the combination of different arguments:

### Specify source directory and output directory

```bash
SuCoS ./my-site --output ./dist
```

In this example, the build command is executed using a custom source directory (`./my-site`). The generated files will be output to the `./dist` directory.

### Combine source, output, and verbose options using the short form

```bash
SuCoS -s ./my-site -o ./dist -v
```

This is a shorter version of the previous example, utilizing the abbreviated forms of the options. The result is the same, with the source directory set to `./my-site`, the output directory set to `./dist`, and verbose output enabled.

### Include draft, future, and expired content

```bash
SuCoS --draft --future --expired
```

This command will build the site with draft content, future-dated content, and expired content included.

### Use default settings with verbose output

```bash
SuCoS -v
```

This command will build the site using the default settings (source directory is `./`, output directory is `./public`), but with verbose output enabled.

### Customize source and include future content

```bash
SuCoS -s ./my-site --future
```

This command will build the site using a custom source directory (`./my-site`) and include future-dated content. The output directory will be the default `./public`.
