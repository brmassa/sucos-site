---
Title: Serve
Params:
  hasCode: true
---

This command unleashes the magic of **SuCoS**, allowing you to serve your website locally for a seamless development experience. The `SuCoS serve` command allows you to preview your static site locally by creating a simple web server. This server does not write any files to the disk; instead, it keeps the site's content in memory. The server also monitors file changes, so it will automatically update the site on each modification.

## Option Details

The serve command includes most of the options available in the build command (except for the `--output` or `-o` option, as the site is served from memory):

- `--verbose` or `-v`: Enables verbose output. This option provides detailed information about the server and build process, which can be helpful for debugging or understanding what's happening behind the scenes.

- `--source` or `-s`: Specifies the source directory path. This is the location where SuCoS will look for your content, templates, and other site-related files. If not specified, the default source directory is the current directory (`./`).

- `--draft` or `-d`: Includes draft content in the server. By default, SuCoS ignores content marked as draft. Use this option if you want to include these files in your local preview.

- `--future` or `-f`: Includes content with dates in the future. By default, SuCoS only includes content with dates in the past or present. Use this option to also include future-dated content in your local preview.

- `--expired` or `-e`: Includes content with expired dates. By default, SuCoS ignores content whose expiration date is in the past. Use this option to include these files in your local preview.

It's important to note that content pages and posts in SuCoS might have the `Date`, `PublishDate`, and `ExpireDate` fields. These fields are used to determine whether a piece of content should be included in the site based on the current date and the options specified (`--draft`, `--future`, `--expired`).

## Command Line Usage Examples

Here are five examples showcasing the usage of the **SuCoS** serve command, demonstrating the combination of different arguments:

### Serve the site with default settings

```bash
SuCoS serve
```

This command will serve the site using the default settings (source directory is `./`).

### Serve the site with verbose output

```bash
SuCoS serve -v
```

This command will serve the site using the default settings (source directory is `./`), but with verbose output enabled.

### Serve the site with a custom source directory

```bash
SuCoS serve -s ./my-site
```

This command will serve the site using a custom source directory (`./my-site`).

### Serve the site with draft and future content

```bash
SuCoS serve --draft --future
```

This command will serve the site with draft content and future-dated content included.

### Serve the site with all content options enabled

```bash
SuCoS serve --draft --future --expired
```

This command will serve the site with draft content, future-dated content, and expired content included.