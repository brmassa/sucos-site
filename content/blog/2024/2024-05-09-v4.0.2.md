---
Title: "v4.0.2 Release: Speed and Stability Boost"
Date: 2024-05-09
Tags:
  - release
Aliases:
  - "v4.0.2"
---

From a user's perspective, this might seem like a simple release, but don't be fooled! We've been hard at work behind the scenes to bring you significant improvements in both speed and stability.

* [JetBrains Rider and Qodana Optimizations](#jetbrains-rider-and-qodana-optimizations)
* [Server Crash on Constant File Changes](#server-crash-on-constant-file-changes)

## JetBrains Rider and Qodana Optimizations

We are excited to announce that JetBrains has kindly provided us with a license for their powerful C# IDE, Rider, and their static code analyzer, Qodana. This has allowed us to dive deep into our codebase and optimize it for even better performance.

## Server Crash on Constant File Changes

Ever noticed that the app would sometimes crash when you were rapidly making changes to a content file? We've tracked down the issue and it turns out that each file change was triggering a duplicate server reload, eventually overwhelming the app. But worry no more - we've fixed this bug, and your rapid-fire edits should now be smooth sailing.

Stay tuned for more exciting updates in the near future!

[Download It Now](https://gitlab.com/sucos/sucos/-/releases/v4.0.2)
