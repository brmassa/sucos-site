# SuCoS site

> [!NOTE]
> This site is generated using SuCoS itself. How cool is that? 🎉

If you are looking for the project itself, go to https://gitlab.com/sucos/sucos.

[![Mastodon](https://img.shields.io/badge/-SuCoS-%232B90D9?style=flat&logo=mastodon&logoColor=white&label=FOSSTODON)](https://fosstodon.org/@SuCoS)
[![Discord](https://img.shields.io/badge/SuCoS-%235865F2.svg?style=flat&logo=discord&logoColor=white)](https://discord.com/channels/1112051704733647058/1112051705316638835)
[![Matrix](https://img.shields.io/badge/SuCoS-%235865F2.svg?style=flat&logo=matrix&logoColor=white)](https://discord.com/channels/1112051704733647058/1112051705316638835)

Welcome to the **SuCoS** project site! This project is all about creating awesome static websites (like this very one) with a touch of magic. With **SuCoS**, you can wave goodbye to complicated setup processes and say hello to simplicity and efficiency.

![MIT License](https://img.shields.io/gitlab/license/sucos%2Fsite)
[![C#](https://img.shields.io/badge/c%23-%23239120.svg?style=plastic&logo=csharp&logoColor=white)](https://img.shields.io/badge/c%23-%23239120.svg?style=plastic&logo=csharp&logoColor=white)

## Features

* Easy Setup: **SuCoS** believes in the power of simplicity. Just a few steps, and you're good to go! No rocket science is required.
* Magic at Your Fingertips: **SuCoS** sprinkles its magic dust to generate beautiful static websites effortlessly. It's like having a digital wizard by your side.
* Customizable Templates: **SuCoS** understands that uniqueness matters. You can easily customize templates to match your style and brand.
* Speedy Performance: **SuCoS** knows that speed is key. It generates lightweight static websites that load like lightning, ensuring a fantastic user experience.
* Markdown Magic: **SuCoS** loves Markdown! Write your content using the power of Markdown and let SuCoS do the rest.

It uses a theme that is in another repository. To update it, run

```sh
git submodule update --recursive --remote 
```

## Contributing

We welcome contributions to the **SuCoS**! If you have any ideas, bug fixes, or feature suggestions, feel free to submit a pull request. Let's make **SuCoS** even more magical together.

This site is powered by the incredible Bootstrap library and hosted on [GitLab Pages](https://gitlab.com/sucos/site), giving it a juicy platform to shine. 🍊

## License and Author

This project is licensed under the **MIT License**, so feel free to use it, modify it, and make it your own.

This SuCoS is authored and maintained by [Bruno Massa](https://brunomassa.com).

Thank you for choosing SuCoS! May your websites be filled with magic and wonder. ✨
